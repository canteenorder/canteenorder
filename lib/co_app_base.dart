import 'dart:async';
import 'dart:io';

//import 'package:path_provider/path_provider.dart';
class COAppBase {
      static bool checkLogin(){
        bool flag = false;
        String hValue = '';
        readSignIn().then((String value){hValue = value;});
        if(hValue.isNotEmpty){
          flag = true;
        }
        return flag;
      }
      static Future<String> get _localPath async {
//        final directory = await getApplicationDocumentsDirectory();
        return ""; //directory.path;
      }
      static Future<File> get _localFile async {
        final path = await _localPath;
        return new File('$path/userdata.txt');
      }
      static Future<String> readSignIn() async {
        try {
          final file = await _localFile;

          // Read the file
          String contents = await file.readAsString();

          return contents;
        } catch (e) {
          // If we encounter an error, return 0
          return '';
        }
      }
}