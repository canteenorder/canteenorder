import 'package:flutter/material.dart';

import './co_app_base.dart';
import './signin_page.dart';

class COApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    bool isSignedIn = COAppBase.checkLogin();
    Object homePage;
    if(isSignedIn){
        // homePage = new SignInPage(title: 'Canteen Order');//Display the items list if signed in
    }else{
      homePage = new SignInPage(title: 'Canteen Order');
    }
    return new MaterialApp(
      title: 'Canteen Order',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.green,
      ),
      home: homePage,
    );
  }
}